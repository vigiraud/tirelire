package main

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
)

func main() {
	store, err := openStore()
	check(err)

	http.HandleFunc("/update", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")

		m, err := parseMutation(r.Body)
		if err != nil {
			http.Error(w, err.Error(), 400)
			return
		}

		err = store.ExecuteMutation(m)
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}
	})

	http.HandleFunc("/list", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")

		values := r.URL.Query()
		public, err := base64.StdEncoding.DecodeString(values.Get("public"))
		if err != nil {
			http.Error(w, err.Error(), 400)
			return
		}

		m, err := store.GetMutation(public)
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		encoder := json.NewEncoder(w)
		_ = encoder.Encode(m)
	})

	fs := http.FileServer(http.Dir("./static"))
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Cache-Control", "max-age=3600, public")
		fs.ServeHTTP(w, r)
	})

	log.Fatal(http.ListenAndServe(":8000", nil))
}

func check(err error) {
	if err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		os.Exit(1)
	}
}
