import settle from './settle.js'
import config from './config.js'

/*
 * General note:
 *
 * l denotes the complete ledger object, retrieved from listLedgers
 * ledger denotes the ledger identifier ('public' in our case)
 */

class Store {
  static uuidv4() {
    return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
      (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    )
  }

  static uuid() {
    return new Date().getTime() + '-' + '00000000'.replace(/0/g, c =>
      (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16))
  }

  static nonce() {
    return nacl.randomBytes(nacl.secretbox.nonceLength)
  }

  static async digest(str) {
    const encoder = new TextEncoder()
    const data = encoder.encode(message)
    return window.crypto.subtle.digest('SHA-256', str)
  }


  static listUsers(entries) {
    const s = new Set()
    entries.forEach(({ user, paidFor }) => {
      if (user) {
        s.add(user)
      }
      (paidFor || []).forEach(u => s.add(u))
    })

    return [...s]
  }

  async db() {
    if (!this._db) {
      this._db = await idb.openDB('tirelire', 1, {
        upgrade(db) {
          const ledgers = db.createObjectStore('ledgers', { keyPath: 'public' })
          const entries = db.createObjectStore('entries', { keyPath: 'uuid' })
          entries.createIndex('ledger', 'ledger')
          entries.createIndex('user', 'user')
          console.log('Database store fully upgraded!')
        },
      })
    }

    return this._db
  }

  async listLedgers() {
    return (await this.db()).getAll('ledgers')
  }

  async addLedger(l) {
    return (await this.db()).add('ledgers', l)
  }

  async rmLedger(ledger) {
    return (await this.db()).delete('ledgers', ledger)
  }

  async listEntries(ledger) {
    return (await this.db()).getAllFromIndex('entries', 'ledger', ledger)
  }

  async putEntry(ledger, entry) {
    return (await this.db()).put('entries', {
      ...entry,
      ledger,
      updated: true,
    })
  }

  async rmEntry(ledger, uuid) {
    return this.putEntry(ledger, {
      uuid,
      deleted: true,
    })
  }

  async report(ledger) {
    const entries = await this.listEntries(ledger)
    const balances = {}
    const totals = {}
    entries.forEach(({ user, paidFor, amount }) => {
      if (!amount || !paidFor || paidFor.length === 0) {
        return
      }

      balances[user] = (balances[user] || 0) + amount
      const dividedAmount = Math.floor(100 * amount / paidFor.length) / 100
      const dividedAmountMain = (amount - (paidFor.length - 1) * dividedAmount).toFixed(2)

      paidFor.forEach((to) => {
        const v = +(to === user ? dividedAmountMain : dividedAmount)
        balances[to] = (balances[to] || 0) - v
        totals[to] = (totals[to] || 0) + v
      })
    })

    const users = Object.keys(balances)
    const rawBalances = users.map(u => balances[u].toFixed(2))
    const rawSettlement = settle(rawBalances)
    const settlement = rawSettlement.map(([from, to, amount]) => ({
      from: users[from],
      to: users[to],
      amount,
    }))

    return { settlement, totals }
  }

  async push(ledger) {
    const [entries, keys] = await Promise.all([
      this.listEntries(ledger),
      this.getKeys(ledger),
    ])

    const ops = []
    entries.forEach((e) => {
      if (e.updated) {
        if (e.deleted) {
          ops.push(Store.getDeletePacket(e, keys.sign.secretKey))
        } else {
          ops.push(Store.getPutPacket(e, keys.encrypt.secretKey, keys.sign.secretKey))
        }
      }
    })

    if (!ops.length) {
      return
    }

    const data = {
      public: ledger,
      ops,
    }
    return fetch(`${config.remoteStore}/update`, { method: 'POST', body: JSON.stringify(data) })
  }

  async pull(ledger) {
    const keys = await this.getKeys(ledger)
    const encoded = encodeURIComponent(ledger)
    const url = `${config.remoteStore}/list?public=${encoded}`
    const result = await (await fetch(url)).json()

    const remoteKeys = new Set(result.ops.map((({ uuid }) => uuid)))

    const tx = (await this.db()).transaction('entries', 'readwrite')
    const localKeys = await tx.store.index('ledger').getAllKeys(ledger)

    // Remove delete localKeys
    localKeys.forEach((uuid) => {
      if (!remoteKeys.has(uuid)) {
        tx.store.delete(uuid)
      }
    })

    // Put (refresh) every keys
    result.ops.forEach((packet) => {
      try {
        tx.store.put({
          ledger,
          ...Store.decryptPutPacket(packet, keys.encrypt.secretKey, keys.sign.publicKey),
        })
      } catch (e) {
        console.error('Unable to decrypt packet', packet, e)
      }
    })

    await tx.done
  }

  async getKeys(ledger) {
    const l = await (await this.db()).get('ledgers', ledger)
    const encrypt = nacl.box.keyPair.fromSecretKey(nacl.util.decodeBase64(l.secret))
    const sign = nacl.sign.keyPair.fromSeed(encrypt.secretKey)

    return {
      encrypt,
      sign,
    }
  }

  async exportLedger(ledger) {
    const l = await (await this.db()).get('ledgers', ledger)
    return btoa(JSON.stringify([
      l.name,
      l.secret,
    ]))
  }

  static newLedger(name) {
    const encrypt = nacl.box.keyPair()
    const sign = nacl.sign.keyPair.fromSeed(encrypt.secretKey)

    return {
      secret: nacl.util.encodeBase64(encrypt.secretKey),
      public: nacl.util.encodeBase64(sign.publicKey),
      name,
    }
  }

  static decodeLedger(raw) {
    const [name, secret] = JSON.parse(atob(raw))
    const rawSecret = nacl.util.decodeBase64(secret)
    const encrypt = nacl.box.keyPair.fromSecretKey(rawSecret)
    const sign = nacl.sign.keyPair.fromSeed(encrypt.secretKey) 

    return {
      public: nacl.util.encodeBase64(sign.publicKey),
      secret,
      name,
    }
  }

  static getPutPacket(entry, encryptKey, signKey) {
    const nonce = this.nonce()
    const packet = JSON.stringify({
      description: entry.description,
      user: entry.user,
      amount: entry.amount,
      paidFor: entry.paidFor,
    })

    const rawPacket = nacl.util.decodeUTF8(packet)
    const cipherText = nacl.secretbox(rawPacket, nonce, encryptKey)
    const signedText = nacl.sign(cipherText, signKey)

    return {
      uuid: entry.uuid,
      nonce: nacl.util.encodeBase64(nonce),
      put: nacl.util.encodeBase64(signedText),
    }
  }

  static decryptPutPacket({ uuid, nonce, put }, encryptKey, signKey) {
    const signedText = nacl.util.decodeBase64(put)
    const cipherText = nacl.sign.open(signedText, signKey)

    const rawNonce = nacl.util.decodeBase64(nonce)
    const rawPacket = nacl.secretbox.open(cipherText, rawNonce, encryptKey)
    const packet = nacl.util.encodeUTF8(rawPacket)
    return {
      uuid,
      ...JSON.parse(packet),
    }
  }

  static getDeletePacket(entry, signKey) {
    const raw = nacl.util.decodeUTF8(entry.uuid)
    const signature = nacl.sign.detached(raw, signKey)

    return {
      uuid: entry.uuid,
      delete: nacl.util.encodeBase64(signature),
    }
  }
}

export default Store;
